import 'mocha';
import * as chai from 'chai';

import { StringUtils } from '../../src/utils/string-utils';

describe('StringUtils のテスト', () => {
  it('isEmpty のテスト', () => {
    chai.assert.equal(StringUtils.isEmpty(''), true);
    chai.assert.equal(StringUtils.isEmpty('ほげほげ'), false);
  });
  it('isExist のテスト', () => {
    chai.assert.equal(StringUtils.isExist(''), false);
    chai.assert.equal(StringUtils.isExist('ほげほげ'), true);
  });
});
