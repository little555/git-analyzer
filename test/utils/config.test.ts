import 'mocha';
import * as chai from 'chai';

import { Config } from '../../src/utils/config';

describe('Config のテスト', () => {
  it('loadConfig のテスト 正常系', () => {
    const target = Config.loadConfig('./test/conf/config_valid.yml');

    chai.assert.equal(target.repository.url, 'https://gitlab.com/little555/git-analyzer.git');
    chai.assert.equal(target.repository.name, 'origin');

    chai.assert.equal(target.analyzers.length, 4);
    chai.assert.equal(target.analyzers[0].type, 'BranchNameAnalyzer');
    chai.assert.deepEqual(target.analyzers[0].params, {
      validBranches: ['origin/master', 'origin/feature/.*']
    });
  });
  it('loadConfig のテスト 異常系 File_NotFound', () => {
    chai.assert.throws(
      () => {
        Config.loadConfig('./test/conf/notfound.yml');
      },
      Error,
      "ENOENT: no such file or directory, open './test/conf/notfound.yml'"
    );
  });
  it('loadConfig のテスト 異常系 Repository_NotFound', () => {
    chai.assert.throws(
      () => {
        Config.loadConfig('./test/conf/config_Repository_NotFound.yml');
      },
      Error,
      '"repository" not found in config file.'
    );
  });
  it('loadConfig のテスト 異常系 Repository_InvalidUrl', () => {
    chai.assert.throws(
      () => {
        Config.loadConfig('./test/conf/config_Repository_InvalidUrl.yml');
      },
      Error,
      '"repository.url" is invalid.'
    );
  });
  it('loadConfig のテスト 異常系 Repository_InvalidName', () => {
    chai.assert.throws(
      () => {
        Config.loadConfig('./test/conf/config_Repository_InvalidName.yml');
      },
      Error,
      '"repository.name" is invalid.'
    );
  });
  it('loadConfig のテスト 異常系 Analyzers_NotFound', () => {
    chai.assert.throws(
      () => {
        Config.loadConfig('./test/conf/config_Analyzers_NotFound.yml');
      },
      Error,
      '"analyzers" not found in config file.'
    );
  });
  it('loadConfig のテスト 異常系 Analyzers_NotArray', () => {
    chai.assert.throws(
      () => {
        Config.loadConfig('./test/conf/config_Analyzers_NotArray.yml');
      },
      Error,
      '"analyzers" is invalid.'
    );
  });
  it('loadConfig のテスト 異常系 Analyzers_InvalidType', () => {
    chai.assert.throws(
      () => {
        Config.loadConfig('./test/conf/config_Analyzers_InvalidType.yml');
      },
      Error,
      '"analyzers[n].type" is invalid.'
    );
  });
  it('loadConfig のテスト 異常系 Analyzers_InvalidParams', () => {
    chai.assert.throws(
      () => {
        Config.loadConfig('./test/conf/config_Analyzers_InvalidParams.yml');
      },
      Error,
      '"analyzers[n].params" is invalid.'
    );
  });
});
