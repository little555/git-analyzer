import 'mocha';
import * as chai from 'chai';

import { DateUtils } from '../../src/utils/date-utils';

describe('DateUtils のテスト', () => {
  it('trimTime のテスト', () => {
    const dateTime = new Date(2017, 7, 24, 12, 12, 12, 12);
    const date = new Date(2017, 7, 24);
    chai.assert.deepEqual(DateUtils.trimTime(dateTime), date);
    chai.assert.deepEqual(DateUtils.trimTime(date), date);
  });
  it('getSpan のテスト', () => {
    chai.assert.equal(DateUtils.getSpan(new Date(2019, 7, 24), new Date(2019, 7, 24)), 0);
    chai.assert.equal(DateUtils.getSpan(new Date(2019, 7, 25), new Date(2019, 7, 24)), 1);
    chai.assert.equal(DateUtils.getSpan(new Date(2019, 8, 1), new Date(2019, 7, 31)), 1);
    chai.assert.equal(DateUtils.getSpan(new Date(2019, 7, 25), new Date(2018, 7, 25)), 365);
    chai.assert.equal(DateUtils.getSpan(new Date(2020, 7, 25), new Date(2019, 7, 25)), 366);
  });
});
