import { BaseAnalyzer } from './analyzer';
import { IBug, Bug } from './bug';
import { DateUtils } from '../utils/date-utils';
import { GitClient, Ibranch } from '../utils/git-client';
import { isNullOrUndefined } from 'util';

interface IParams {
  srcBranch: string;
  targetBranches: string[];
  lifespan: number;
}

/**
 * ブランチがマージされずに伸びすぎていないかを確認します
 */
export class BranchLifespanAnalyzer extends BaseAnalyzer {
  constructor(gitClient: GitClient, private params: IParams) {
    super(gitClient);
  }

  async analyze(): Promise<IBug[]> {
    console.log(`run ${this.constructor.name}`);
    console.log(this.params);

    const branches = await this.gitClient.branch();
    const targetReg = new RegExp(this.params.targetBranches.join('|'));
    const srcBranch = branches.find(b => b.name === this.params.srcBranch);
    const targetBranches = branches.filter(b => targetReg.test(b.name));

    let bugs: IBug[] = [];
    if (srcBranch !== undefined) {
      for (const branch of targetBranches) {
        bugs = bugs.concat(await this.analyzeBranch(srcBranch, branch));
      }
    }
    return bugs;
  }

  private async analyzeBranch(srcBranch: Ibranch, branch: Ibranch): Promise<IBug[]> {
    let bugs: IBug[] = [];
    const branchedDateTime = await this.gitClient.getBranchedDateTime(srcBranch.name, branch.name);

    if (!isNullOrUndefined(branchedDateTime)) {
      const targetAge = DateUtils.getSpan(branch.commitDateTime, branchedDateTime);
      if (targetAge > this.params.lifespan) {
        bugs.push(
          new Bug(
            `${branch.name} branched from ${srcBranch.name} is not merged. ${
              branch.name
            } has branched ${targetAge} days from the date of derivation.`,
            this.constructor.name
          )
        );
      }
      const srcAge = DateUtils.getSpan(srcBranch.commitDateTime, branchedDateTime);
      if (srcAge > this.params.lifespan) {
        bugs.push(
          new Bug(
            `${branch.name} branched from ${srcBranch.name} is not merged. ${
              srcBranch.name
            } has branched ${srcAge} days from the date of derivation.`,
            this.constructor.name
          )
        );
      }
    } else {
      bugs.push(new Bug(`Unable to get the date and time when ${branch.name} branched.`, this.constructor.name));
    }

    return bugs;
  }
}
