import { AnalyzerConfig } from '../utils/config';
import { IAnalyzer } from './analyzer';
import { GitClient } from '../utils/git-client';
import { BranchNameAnalyzer } from './branch-name-analyzer';
import { BranchLifespanAnalyzer } from './branch-lifespan-analyzer';
import { DeadBranchAnalyzer } from './dead-branch-analyzer';
import { ConflictAnalyzer } from './conflict-analyzer';
import { MergedBranchAnalyzer } from './merged-branch-analyzer';

export class AnalyzerFactory {
  static createAnalyzers(gitClient: GitClient, analyzerConfigs: AnalyzerConfig[]): IAnalyzer[] {
    const analyzers: IAnalyzer[] = [];
    for (const analyzerData of analyzerConfigs) {
      const a = AnalyzerFactory.createAnalyzer(gitClient, analyzerData);
      if (a !== undefined) {
        analyzers.push(a);
      }
    }
    return analyzers;
  }

  private static createAnalyzer(gitClient: GitClient, analyzerConf: AnalyzerConfig): IAnalyzer {
    switch (analyzerConf.type) {
      case 'BranchNameAnalyzer':
        return new BranchNameAnalyzer(gitClient, analyzerConf.params);
      case 'BranchLifespanAnalyzer':
        return new BranchLifespanAnalyzer(gitClient, analyzerConf.params);
      case 'DeadBranchAnalyzer':
        const date = new Date();
        return new DeadBranchAnalyzer(gitClient, { ...analyzerConf.params, toDay: date });
      case 'ConflictAnalyzer':
        return new ConflictAnalyzer(gitClient, analyzerConf.params);
      case 'MergedBranchAnalyzer':
        return new MergedBranchAnalyzer(gitClient, analyzerConf.params);
      default:
        throw new Error(`unknown analyzer "${analyzerConf.type}"`);
    }
  }
}
