import { BaseAnalyzer } from './analyzer';
import { IBug, Bug } from './bug';
import { GitClient, Ibranch } from '../utils/git-client';
import { isUndefined } from 'util';

interface IParams {
  srcBranch: string;
  targetBranches: string[];
}

export class ConflictAnalyzer extends BaseAnalyzer {
  constructor(gitClient: GitClient, private params: IParams) {
    super(gitClient);
  }

  async analyze(): Promise<IBug[]> {
    console.log(`run ${this.constructor.name}`);
    console.log(this.params);

    const branches = await this.gitClient.branch();
    const targetReg = new RegExp(this.params.targetBranches.join('|'));
    const srcBranch = branches.find(b => b.name === this.params.srcBranch);
    const targetBranches = branches.filter(b => targetReg.test(b.name));

    let bugs: IBug[] = [];
    if (!isUndefined(srcBranch)) {
      for (const branch of targetBranches) {
        bugs = bugs.concat(await this.analyzeBranch(srcBranch, branch));
      }
    }
    return bugs;
  }

  private async analyzeBranch(srcBranch: Ibranch, branch: Ibranch): Promise<IBug[]> {
    let bugs: IBug[] = [];
    const conflictFiles: string[] = await this.gitClient.getConflictFiles(srcBranch, branch);
    if (conflictFiles.length > 0) {
      bugs.push(new Bug(`has conflict files. [${conflictFiles}]`, this.constructor.name));
    }
    return bugs;
  }
}
