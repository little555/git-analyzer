import { BaseAnalyzer } from './analyzer';
import { IBug, Bug } from './bug';
import { GitClient, Ibranch } from '../utils/git-client';

interface IParams {
  srcBranch: string;
  targetBranches: string[];
}

export class MergedBranchAnalyzer extends BaseAnalyzer {
  constructor(gitClient: GitClient, private params: IParams) {
    super(gitClient);
  }

  async analyze(): Promise<IBug[]> {
    console.log(`run ${this.constructor.name}`);
    console.log(this.params);

    const branches = await this.gitClient.branch();
    const targetReg = new RegExp(this.params.targetBranches.join('|'));

    const mergedBranches = await this.gitClient.getMergedBranches(this.params.srcBranch);

    return mergedBranches
      .filter(b => targetReg.test(b.name))
      .map(b => new Bug(`${b.name} is already merged. Please delete it.`, this.constructor.name));
  }
}
