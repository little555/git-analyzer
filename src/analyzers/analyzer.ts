import { IBug } from "./bug";
import { GitClient } from "../utils/git-client";

export interface IAnalyzer {
  analyze(): Promise<IBug[]>;
}

export abstract class BaseAnalyzer implements IAnalyzer {
  constructor(protected gitClient: GitClient) { }
  abstract async analyze(): Promise<IBug[]>;
}