import yaml from 'js-yaml';
import fs from 'fs';
import { isNullOrUndefined, isArray, isString } from 'util';
import { checkServerIdentity } from 'tls';

export class RepositoryConfig {
  constructor(private readonly _repoDoc: any) {
    RepositoryConfig.checkParams(_repoDoc);
  }

  private static checkParams(_repoDoc: any) {
    if (!isString(_repoDoc.url)) {
      throw new Error('"repository.url" is invalid.');
    }
    if (!isString(_repoDoc.name)) {
      throw new Error('"repository.name" is invalid.');
    }
  }
  get url(): string {
    return this._repoDoc.url;
  }
  get name(): string {
    return this._repoDoc.name;
  }
}

export class AnalyzerConfig {
  constructor(private readonly _anaDoc: any) {
    AnalyzerConfig.checkParams(_anaDoc);
  }

  private static checkParams(_anaDoc: any) {
    if (!isString(_anaDoc.type)) {
      throw new Error('"analyzers[n].type" is invalid.');
    }
    if (isNullOrUndefined(_anaDoc.params)) {
      throw new Error('"analyzers[n].params" is invalid.');
    }
  }

  get type(): string {
    return this._anaDoc.type;
  }
  get params(): any {
    return this._anaDoc.params;
  }
}

export class Config {
  private constructor(private readonly _repository: RepositoryConfig, private readonly _analyzers: AnalyzerConfig[]) {}

  static loadConfig(configPath: string): Config {
    var doc = yaml.safeLoad(fs.readFileSync(configPath, 'utf8'));
    if (isNullOrUndefined(doc)) {
      throw new Error(`${configPath} load faild.`);
    }

    Config.checkDocParams(doc);
    const repo = new RepositoryConfig(doc.repository);
    const ana = doc.analyzers.map((a: any) => new AnalyzerConfig(a));
    return new Config(repo, ana);
  }

  private static checkDocParams(doc: any) {
    if (isNullOrUndefined(doc.repository)) {
      throw new Error('"repository" not found in config file.');
    }
    if (isNullOrUndefined(doc.analyzers)) {
      throw new Error('"analyzers" not found in config file.');
    }
    if (!isArray(doc.analyzers)) {
      throw new Error('"analyzers" is invalid.');
    }
  }

  get repository(): RepositoryConfig {
    return this._repository;
  }
  get analyzers(): AnalyzerConfig[] {
    return this._analyzers;
  }
}
