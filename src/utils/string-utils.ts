export class StringUtils {
  static isEmpty(str: string): boolean {
    return str === '';
  }
  static isExist(str: string): boolean {
    return !StringUtils.isEmpty(str);
  }
}
