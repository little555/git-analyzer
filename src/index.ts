import { IAnalyzer } from './analyzers/analyzer';
import { IBug } from './analyzers/bug';
import { Config } from './utils/config';
import { GitClient } from './utils/git-client';
import { AnalyzerFactory } from './analyzers/analyzer-factory';

// setting.jsonに移植する予定の奴ら
const MAIN_BRANCH = 'origin/master';
const FEATURE_BRANCH_REGEX = 'origin/feature/.*';

(async () => {
  console.log(`## load config`);
  const config = Config.loadConfig('./conf/config.yml');
  if (config === null) {
    console.log(`config load faild`);
    process.exit(1);
  }

  console.log(`## init GitClient`);
  const gitClient = new GitClient(config.repository);
  await gitClient.init();

  // 解析処理
  console.log(`## create Analyzer`);
  const analyzers = AnalyzerFactory.createAnalyzers(gitClient, config.analyzers);

  console.log(`## analyze`);
  let bugs: IBug[] = [];
  for (const analyzer of analyzers) {
    bugs = bugs.concat(await analyzer.analyze());
  }
  console.log(`## result`);
  for (const bug of bugs) {
    console.log(bug.toString());
  }
})();
